<?php

namespace Drupal\graphql_group_menu\Plugin\GraphQL\SchemaExtension;

use Drupal\Core\Menu\MenuLinkInterface;
use Drupal\Core\Menu\MenuLinkTreeElement;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\Url;
use Drupal\graphql\GraphQL\ResolverBuilder;
use Drupal\graphql\GraphQL\ResolverRegistry;
use Drupal\graphql\GraphQL\ResolverRegistryInterface;
use Drupal\graphql\Plugin\GraphQL\SchemaExtension\SdlSchemaExtensionPluginBase;
use Drupal\group_content_menu\Entity\GroupContentMenu;
use Drupal\group_content_menu\GroupContentMenuInterface;
use Drupal\menu_link_content\MenuLinkContentInterface;

/**
 * @SchemaExtension(
 *   id = "menu_extension",
 *   name = "Menu extension",
 *   description = "Add menu fields.",
 *   schema = "graphql"
 * )
 */
class MenuExtension extends SdlSchemaExtensionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function registerResolvers(ResolverRegistryInterface $registry): void {
    $builder = new ResolverBuilder();

    $this->addQueryFields($registry, $builder);
    $this->addMenuFields($registry, $builder);
  }

  /**
   * @param \Drupal\graphql\GraphQL\ResolverRegistry $registry
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   */
  protected function addMenuFields(ResolverRegistry $registry, ResolverBuilder $builder): void {
    // Menu name.
    $registry->addFieldResolver(
      'Menu',
      'name',
      $builder->callback(
        function (GroupContentMenu $groupContentMenu) {
          return $groupContentMenu->label();
        }
      )
    );

    // Menu items.
    $registry->addFieldResolver(
      'Menu',
      'items',
      $builder->compose(
        $builder->callback(
          function (GroupContentMenu $groupContentMenu) use ($builder) {
            $tree = \Drupal::menuTree()
              ->load(GroupContentMenuInterface::MENU_PREFIX . $groupContentMenu->id(), new MenuTreeParameters());

            $manipulators = [
              ['callable' => 'menu.default_tree_manipulators:checkAccess'],
              ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
            ];

            return array_filter(
              \Drupal::menuTree()
                ->transform($tree, $manipulators),
              function (MenuLinkTreeElement $item) {
                return $item->link instanceof MenuLinkInterface && $item->link->isEnabled();
              }
            );
          }
        ),
        $builder->produce('filter_menu_items_language')
          ->map('items', $builder->fromParent())
          ->map('language', $builder->fromContext('language'))
      )
    );

    // Menu title.
    $registry->addFieldResolver(
      'MenuItem',
      'title',
      $builder->compose(
        $builder->produce('menu_tree_link')
          ->map('element', $builder->fromParent()),
        $builder->produce('menu_link_translation')
          ->map('link', $builder->fromParent())
          ->map('language', $builder->fromContext('language')),
        $builder->callback(
          function (?MenuLinkContentInterface $menuLinkContent) {
            return $menuLinkContent->label();
          }
        )
      )
    );

    // Menu children.
    $registry->addFieldResolver(
      'MenuItem',
      'children',
      $builder->produce('menu_tree_subtree')
        ->map('element', $builder->fromParent())
    );

    // Menu url.
    $registry->addFieldResolver(
      'MenuItem',
      'url',
      $builder->compose(
        $builder->produce('menu_tree_link')
          ->map('element', $builder->fromParent()),
        $builder->produce('menu_link_translation')
          ->map('link', $builder->fromParent())
          ->map('language', $builder->fromContext('language')),
        $builder->callback(
          function (?MenuLinkContentInterface $menuLinkContent) {
            if (!$menuLinkContent) {
              return Url::fromRoute('<front>');
            }
            $url = $menuLinkContent->getUrlObject();
            $override = $menuLinkContent->get('link_override')->getValue();
            $override = reset($override);

            if ($override && $uri = $override['uri']) {
              $url = Url::fromUri($uri, $override['options']);
            }

            $route_params = $url->isRouted() ? $url->getRouteParameters() : NULL;

            if (!$route_params) {
              return $url;
            }

            $entity_type = key($route_params);
            $entity = \Drupal::entityTypeManager()->getStorage($entity_type)->load($route_params[$entity_type]);

            if (!$entity) {
              return $url;
            }

            $langcode = $menuLinkContent->language()->getId();

            $entity = $entity->hasTranslation($langcode) ?
              $entity->getTranslation($langcode) :
              $entity;

            return $entity->toUrl();
          }
        )
      )
    );

    $registry->addFieldResolver(
      'Url',
      'path',
      $builder->produce('url_path')
        ->map('url', $builder->fromParent())
    );
  }

  /**
   * @param \Drupal\graphql\GraphQL\ResolverRegistry $registry
   * @param \Drupal\graphql\GraphQL\ResolverBuilder $builder
   */
  protected function addQueryFields(ResolverRegistry $registry, ResolverBuilder $builder): void {
    // Menu query.
    $registry->addFieldResolver(
      'Query',
      'menu',
      $builder->compose(
        $builder->produce('menu_by_group_content')
          ->map('groupId', $builder->fromArgument('groupId'))
          ->map('bundle', $builder->fromArgument('name'))
          ->map('language', $builder->fromArgument('language')),
        $builder->context('language', $builder->fromArgument('language'))
      ),
    );

    $registry->addFieldResolver(
      'Query',
      'menuGlobal',
      $builder->compose(
        $builder->produce('menu_global')
          ->map('menu', $builder->fromArgument('menu'))
          ->map('language', $builder->fromArgument('language')),
        $builder->produce('filter_menu_items_language')
          ->map('items', $builder->fromParent())
          ->map('language', $builder->fromArgument('language')),
        $builder->context('language', $builder->fromArgument('language'))
      ),
    );
  }

}
