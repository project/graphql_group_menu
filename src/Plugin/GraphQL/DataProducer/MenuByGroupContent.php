<?php

namespace Drupal\graphql_group_menu\Plugin\GraphQL\DataProducer;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\Annotation\DataProducer;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupInterface;
use Drupal\group_content_menu\Entity\GroupContentMenu;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns the URL string of an url.
 *
 * @DataProducer(
 *   id = "menu_by_group_content",
 *   name = @Translation("Get the menu by the group content"),
 *   description = @Translation("Returns the menu entity."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Menu by group content")
 *   ),
 *   consumes = {
 *     "groupId" = @ContextDefinition("string",
 *       label = @Translation("Group ID")
 *     ),
 *     "bundle" = @ContextDefinition("string",
 *       label = @Translation("Bundle")
 *     ),
 *     "language" = @ContextDefinition("string",
 *       label = @Translation("Language")
 *     ),
 *   }
 * )
 */
class MenuByGroupContent extends DataProducerPluginBase implements ContainerFactoryPluginInterface {
  use DependencySerializationTrait;

  /**
   * The menu link tree.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected $menuLinkTree;

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('menu.link_tree')
    );
  }

  /**
   * MenuItems constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param \Drupal\Core\Menu\MenuLinkTreeInterface $menuLinkTree
   *   The menu link tree service.
   *
   * @codeCoverageIgnore
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, MenuLinkTreeInterface $menuLinkTree) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->menuLinkTree = $menuLinkTree;
  }

  /**
   * @param int $groupId
   * @param string $bundle
   * @param string $language
   *
   * @return \Drupal\group_content_menu\Entity\GroupContentMenu|null
   */
  public function resolve(string $groupId, string $bundle, string $language): ?GroupContentMenu {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = \Drupal::service('entity_type.manager');
    $storage_handler = $entity_type_manager->getStorage('group');

    $group = $storage_handler->loadByProperties(
      [
        'field_group_id' => $groupId,
      ]
    );

    if (empty($group)) {
      return NULL;
    }

    $group = reset($group);

    if (!$group instanceof GroupInterface) {
      return NULL;
    }

    $group_contents = group_content_menu_get_menus_per_group($group);

    foreach ($group_contents as $group_content) {
      /** @var GroupContentMenu $content_menu */
      $content_menu = $group_content->getEntity();

      if ($content_menu->bundle() === $bundle) {
        return $content_menu;
      }
    }

    return NULL;
  }

}
