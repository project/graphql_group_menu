<?php

namespace Drupal\graphql_group_menu\Plugin\GraphQL\DataProducer;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Menu\MenuLinkInterface;
use Drupal\graphql\Annotation\DataProducer;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * Get translation of the current menu link.
 *
 * @DataProducer(
 *   id = "menu_link_translation",
 *   name = @Translation("Get translation of the current menu link."),
 *   produces = @ContextDefinition("menu",
 *     label = @Translation("Menu")
 *   ),
 *   consumes = {
 *     "link" = @ContextDefinition("entity",
 *       label = @Translation("Link")
 *     ),
 *     "language" = @ContextDefinition("string",
 *       label = @Translation("Entity language"),
 *       required = TRUE
 *     ),
 *   }
 * )
 */
class MenuLinkTranslation extends DataProducerPluginBase{

  /**
   * @param MenuLinkInterface $menuLinkContent
   * @param string $language
   *
   * @return \Drupal\menu_link_content\MenuLinkContentInterface|null
   */
  public function resolve(MenuLinkInterface $menuLinkContent, string $language): ?ContentEntityInterface {
    $uuid = $menuLinkContent->getDerivativeId();
    /** @var \Drupal\menu_link_content\MenuLinkContentInterface $menuLinkContent */
    $menuLinkContent = \Drupal::service('entity.repository')
      ->loadEntityByUuid('menu_link_content', $uuid);
    return $menuLinkContent->hasTranslation($language) ?
      $menuLinkContent->getTranslation($language) :
      NULL;
  }

}
