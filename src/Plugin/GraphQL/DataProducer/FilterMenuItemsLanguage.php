<?php

namespace Drupal\graphql_group_menu\Plugin\GraphQL\DataProducer;

use Drupal\Core\Menu\InaccessibleMenuLink;
use Drupal\graphql\Annotation\DataProducer;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;

/**
 * Filter menu items by language
 *
 * @DataProducer(
 *   id = "filter_menu_items_language",
 *   name = @Translation("Filter menu items by language"),
 *   description = @Translation(""),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Menu by group content")
 *   ),
 *   consumes = {
 *     "items" = @ContextDefinition("list",
 *       label = @Translation("Items")
 *     ),
 *     "language" = @ContextDefinition("string",
 *       label = @Translation("Language")
 *     )
 *   }
 * )
 */
class FilterMenuItemsLanguage extends DataProducerPluginBase {

  /**
   * @param array|null $items
   * @param string|null $language
   *
   * @return array|null
   */
  public function resolve(?array $items, ?string $language): ?array {
    return array_filter($items, function ($item) use ($language) {
      /** @var \Drupal\menu_link_content\Plugin\Menu\MenuLinkContent $link */
      $link = $item->link;

      if (!$link || $link instanceof InaccessibleMenuLink) {
        return FALSE;
      }

      $uuid = $link->getDerivativeId();
      /** @var \Drupal\menu_link_content\MenuLinkContentInterface $menuLinkContent */
      $menuLinkContent = \Drupal::service('entity.repository')
        ->loadEntityByUuid('menu_link_content', $uuid);

      if (empty($menuLinkContent)) {
        return FALSE;
      }

      return $menuLinkContent->hasTranslation($language);
    });
  }

}
