<?php

namespace Drupal\graphql_group_menu\Plugin\GraphQL\DataProducer;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Menu\MenuLinkInterface;
use Drupal\Core\Menu\MenuLinkTreeElement;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\Annotation\DataProducer;
use Drupal\graphql\Plugin\GraphQL\DataProducer\DataProducerPluginBase;
use Drupal\group_content_menu\Entity\GroupContentMenu;
use Drupal\system\Entity\Menu;
use Drupal\system\MenuInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns the URL string of an url.
 *
 * @DataProducer(
 *   id = "menu_global",
 *   name = @Translation("Get the menu global"),
 *   description = @Translation("Returns the menu entity."),
 *   produces = @ContextDefinition("any",
 *     label = @Translation("Menu by group content")
 *   ),
 *   consumes = {
 *     "menu" = @ContextDefinition("string",
 *       label = @Translation("menu")
 *     ),
 *     "language" = @ContextDefinition("string",
 *       label = @Translation("language")
 *     ),
 *   }
 * )
 */
class MenuGlobal extends DataProducerPluginBase implements ContainerFactoryPluginInterface {
  use DependencySerializationTrait;

  /**
   * The menu link tree.
   *
   * @var \Drupal\Core\Menu\MenuLinkTreeInterface
   */
  protected $menuLinkTree;

  /**
   * {@inheritdoc}
   *
   * @codeCoverageIgnore
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('menu.link_tree')
    );
  }

  /**
   * MenuItems constructor.
   *
   * @param array $configuration
   *   The plugin configuration array.
   * @param string $pluginId
   *   The plugin id.
   * @param mixed $pluginDefinition
   *   The plugin definition.
   * @param \Drupal\Core\Menu\MenuLinkTreeInterface $menuLinkTree
   *   The menu link tree service.
   *
   * @codeCoverageIgnore
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, MenuLinkTreeInterface $menuLinkTree) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->menuLinkTree = $menuLinkTree;
  }

  /**
   * @param string $menu
   * @param string $language
   *
   * @return array|null
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function resolve(string $menu, string $language): ?array {
    $menu = Menu::load($menu);
    $tree = $this->menuLinkTree->load($menu->id(), new MenuTreeParameters());

    $manipulators = [
      ['callable' => 'menu.default_tree_manipulators:checkAccess'],
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ];

    return array_filter(
      $this->menuLinkTree->transform($tree, $manipulators),
      function (MenuLinkTreeElement $item) use ($language) {
        $url = $item->link->getUrlObject();
        $route_params = $url->isRouted() ? $url->getRouteParameters() : NULL;

        $is_published = TRUE;

        if ($route_params) {
          $entity_type = key($route_params);
          /** @var \Drupal\node\NodeInterface $entity */
          $entity = \Drupal::entityTypeManager()->getStorage($entity_type)->load($route_params[$entity_type]);

          if ($entity) {
            $entity = $entity->hasTranslation($language) ?
              $entity->getTranslation($language) :
              $entity;

            $is_published = $entity->isPublished();
          }
        }

        return $item->link instanceof MenuLinkInterface && $item->link->isEnabled() && $is_published;
      }
    );
  }

}
